/**
 * 
 */

/**
 * @author kyle_
 *
 */
import java.lang.Math;

public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		return Math.pow(arg1, arg2);
	}

	@Override
	public String getName() {
		return "^";
	}
	
}
