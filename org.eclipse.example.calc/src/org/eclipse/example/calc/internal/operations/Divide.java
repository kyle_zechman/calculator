/**
 * 
 */

/**
 * @author kyle_
 *
 */
import org.eclipse.example.calc.BinaryOperation;

public class Divide extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		return arg1 / arg2;
	}

	@Override
	public String getName() {
		return "/";
	}

=======
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;


public class Divide extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		if (arg2 == 0){
			return System.out.println("Can not divide by zero");
		}
		else {
			return arg1 / arg2;
		}
		
	}

	@Override
	public String getName() {
		return "/";
	}

}
